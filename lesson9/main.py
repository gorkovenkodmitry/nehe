#coding: utf-8
import random
from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *
from PIL import Image

ESCAPE = '\033'

window = 0
twinkle = False
num = 50
stars = []
zoom = -15
tilt = 90
spin = 0
texture = None


class Star:
    angle = 0
    dist = 0
    r = 0
    g = 0
    b = 0

    def __init__(self, dist, angle, r, g, b):
        self.angle = angle
        self.dist = dist
        self.r = r
        self.g = g
        self.b = b

    def __unicode__(self):
        return u'%s, %s, %s' % (self.r, self.g, self.b)


def load_textures(filename):
    img = Image.open(filename)
    size = img.size
    img = img.convert('RGBA').tostring('raw', 'RGBA', 0, -1)
    texture = glGenTextures(1)
    glBindTexture(GL_TEXTURE_2D, texture)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glTexImage2D(GL_TEXTURE_2D, 0, 4, size[0], size[1], 0, GL_RGBA, GL_UNSIGNED_BYTE, img)
    return texture


def init_gl(width, height):
    global texture
    texture = load_textures('Star.bmp')
    glEnable(GL_TEXTURE_2D)
    glShadeModel(GL_SMOOTH)
    glClearColor(0.0, 0.0, 0.0, 0.5)
    glClearDepth(1)

    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE)
    glEnable(GL_BLEND)

    for i in range(num):
        star = Star(5.0*i/float(num), 0, random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
        stars.append(star)


def resize_gl_scene(width, height):
    if height == 0:
        height = 1

    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0, float(width)/float(height), 0.1, 100.0)
    glMatrixMode(GL_MODELVIEW)


def draw_gl_scene():
    global texture, stars, zoom, tilt, spin, twinkle
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glBindTexture(GL_TEXTURE_2D, texture)
    for i in range(len(stars)):
        glLoadIdentity()
        glTranslatef(0, 0, zoom)
        glRotatef(tilt, 1, 0, 0)
        glRotatef(stars[i].angle, 0, 1, 0)
        glTranslatef(stars[i].dist, 0, 0)
        glRotatef(-stars[i].angle, 0, 1, 0)
        glRotatef(-tilt, 1, 0, 0)

        if twinkle:
            glColor4ub(stars[len(stars) - i - 1].r, stars[len(stars) - i - 1].g, stars[len(stars) - i - 1].b, 255)
            glBegin(GL_QUADS)
            glTexCoord2f(0.0, 0.0)
            glVertex3f(-1.0, -1.0, 0.0)
            glTexCoord2f(1.0, 0.0)
            glVertex3f(1.0, -1.0, 0.0)
            glTexCoord2f(1.0, 1.0)
            glVertex3f(1.0, 1.0, 0.0)
            glTexCoord2f(0.0, 1.0)
            glVertex3f(-1.0, 1.0, 0.0)
            glEnd()


        glRotatef(spin, 0, 0, 1)
        glColor4ub(stars[i].r, stars[i].g, stars[i].b, 255)

        glBegin(GL_QUADS)
        glTexCoord2f(0.0, 0.0)
        glVertex3f(-1.0, -1.0, 0.0)
        glTexCoord2f(1.0, 0.0)
        glVertex3f(1.0, -1.0, 0.0)
        glTexCoord2f(1.0, 1.0)
        glVertex3f(1.0, 1.0, 0.0)
        glTexCoord2f(0.0, 1.0)
        glVertex3f(-1.0, 1.0, 0.0)
        glEnd()

        spin += .01
        stars[i].angle += i / float(len(stars))
        stars[i].dist -= .01
        if stars[i].dist < 0:
            stars[i].dist += 5
            stars[i].r = random.randint(0, 255)
            stars[i].g = random.randint(0, 255)
            stars[i].b = random.randint(0, 255)

    glutSwapBuffers()


def key_pressed(*args):
    global twinkle, zoom
    if args[0] == 'x':
        zoom += .1
    if args[0] == 'z':
        zoom -= .1
    if args[0] == 't':
        twinkle = not twinkle
    if args[0] == ESCAPE:
        glutDestroyWindow(window)
        sys.exit()


def special_key_pressed(*args):
    print args


def main():
    global window
    glutInit("")

    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH)

    glutInitWindowSize(640, 480)

    glutInitWindowPosition(0, 0)

    window = glutCreateWindow("lesson 9")

    glutDisplayFunc(draw_gl_scene)

    # Раскоментировать для полноэкранного режима
    # glutFullScreen()

    glutIdleFunc(draw_gl_scene)

    glutReshapeFunc(resize_gl_scene)

    glutKeyboardFunc(key_pressed)
    glutSpecialFunc(special_key_pressed)

    init_gl(640, 480)

    glutMainLoop()

print "Hit ESC key to quit."
main()