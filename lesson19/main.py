#coding: utf-8
import random
from OpenGL.GL import *
from OpenGL.GL.VERSION.GL_1_1 import glGenTextures
from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.raw.GL.VERSION.GL_1_1 import glBindTexture
from PIL import Image

ESCAPE = '\033'
MAX_PARTICLES = 1000

window = 0
textures = []
light = False
lp = False
fp = False
xrot = yrot = xspeed = yspeed = 0
light_ambient = [.5, .5, .5, 1]
light_diffuse = [1, 1, 1, 1]
light_position = [0, 0, 2, 1]
filter_type = 0
active = True
slowdown = 2
zoom = -40
loop = 0
col = 0
delay = 0
colors = [
    [1, .5, .5],
    [1, .75, .5],
    [1, 1, .5],
    [.75, 1, .5],

    [.5, 1, .5],
    [.5, 1, .75],
    [.5, 1, 1],
    [.5, .75, 1],

    [.5, .5, 1],
    [.75, .5, 1],
    [1, .5, 1],
    [1, .5, .75],
]
particles = []


class Particle:

    def __init__(self, active, life, fade, r, g, b, x, y, z, xi, yi, zi, xg, yg, zg):
        self.active = active
        self.life = life
        self.fade = fade
        self.r = r
        self.g = g
        self.b = b
        self.x = x
        self.y = y
        self.z = z
        self.xi = xi
        self.yi = yi
        self.zi = zi
        self.xg = xg
        self.yg = yg
        self.zg = zg

    def get_coords(self):
        return self.x, self.y, self.z


def load_textures(filter_type, filename):
    img = Image.open(filename)
    size = img.size
    img = img.convert('RGBA').tobytes('raw', 'RGBA', 0, -1)
    texture = glGenTextures(1)
    glBindTexture(GL_TEXTURE_2D, texture)
    if filter_type == 0:
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    elif filter_type == 1:
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    elif filter_type == 2:
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST)
    else:
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    # glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL)
    if filter_type != 2:
        glTexImage2D(GL_TEXTURE_2D, 0, 4, size[0], size[1], 0, GL_RGBA, GL_UNSIGNED_BYTE, img)
    else:
        gluBuild2DMipmaps(GL_TEXTURE_2D, 4, size[0], size[1], GL_RGBA, GL_UNSIGNED_BYTE, img)
    return texture


def init_gl(width, height):
    global textures, filter_type, light_ambient, light_diffuse, light_position, MAX_PARTICLES, particles
    for i in range(3):
        textures.append(load_textures(i, 'Particle.bmp'))
    glShadeModel(GL_SMOOTH)
    glClearColor(0.0, 0.0, 0.0, 0.0)
    glClearDepth(1.0)
    glDisable(GL_DEPTH_TEST)
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE)
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST)
    glHint(GL_POINT_SMOOTH_HINT,GL_NICEST)
    glEnable(GL_TEXTURE_2D)

    # свет
    glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient)
    glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffuse)
    glLightfv(GL_LIGHT1, GL_POSITION, light_position)
    glEnable(GL_LIGHT1)

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0, float(width)/float(height), 0.1, 200.0)

    glMatrixMode(GL_MODELVIEW)

    for i in range(MAX_PARTICLES):
        particles.append(
            Particle(
                True,
                1,
                random.random()/2.0,
                colors[int(i*12/float(MAX_PARTICLES))][0],
                colors[int(i*12/float(MAX_PARTICLES))][1],
                colors[int(i*12/float(MAX_PARTICLES))][2],
                0,
                0,
                0,
                (random.randint(1, 50) - 25)*10.0,
                (random.randint(1, 50) - 25)*10.0,
                (random.randint(1, 50) - 25)*10.0,
                0,
                0,
                0
            )
        )


def resize_gl_scene(width, height):
    if height == 0:
        height = 1

    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0, float(width)/float(height), 0.1, 100.0)
    glMatrixMode(GL_MODELVIEW)


def draw_gl_scene():
    global xrot, yrot, xspeed, yspeed, zoom, textures, filter_type, slowdown, particles
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    glLoadIdentity()
    glTranslatef(0, 0, zoom)
    # куб
    glBindTexture(GL_TEXTURE_2D, textures[filter_type])

    for ind, particle in enumerate(particles):
        if particle.active:
            x, y, z = particle.get_coords()
            glColor4f(particle.r, particle.g, particle.b, particle.life)

            glBegin(GL_TRIANGLE_STRIP)
            glTexCoord2d(1, 1)
            glVertex3f(x+0.5, y+0.5, z) # Верхняя правая
            glTexCoord2d(0, 1)
            glVertex3f(x-0.5, y+0.5, z) # Верхняя левая
            glTexCoord2d(1, 0) 
            glVertex3f(x+0.5, y-0.5, z) # Нижняя правая 
            glTexCoord2d(0, 0)
            glVertex3f(x-0.5, y-0.5, z) # Нижняя левая
            glEnd()

            particle.x += particle.xi/float(slowdown*1000)
            particle.y += particle.yi/float(slowdown*1000)
            particle.z += particle.zi/float(slowdown*1000)

            particle.xi += particle.xg
            particle.yi += particle.yg
            particle.zi += particle.zg

            particle.life -= particle.fade

            if particle.life < 0:
                particle.life = 1
                particle.fade = random.random()/2.0
                particle.x = 0
                particle.y = 0
                particle.z = 0
                particle.xi = xspeed + random.randint(1, 60)-32
                particle.yi = yspeed + random.randint(1, 60)-30
                particle.zi = random.randint(1, 60)-30
                col = random.randint(0, 11)
                particle.r = colors[col][0]
                particle.g = colors[col][1]
                particle.b = colors[col][2]

    glutSwapBuffers()
    xrot += xspeed
    yrot += yspeed


def key_pressed(*args):
    global textures, filter_type, light, zoom, xspeed, yspeed, slowdown
    if args[0] == 'f':
        filter_type = (filter_type + 1) % 3
        print filter_type
    if args[0] == 'l':
        light = not light
        if light:
            glEnable(GL_LIGHTING)
        else:
            glDisable(GL_LIGHTING)
    if args[0] == ',':
        if slowdown < 40:
            slowdown += 0.1
    if args[0] == '.':
        if slowdown > -40:
            slowdown -= 0.1
    if args[0] == 'x':
        zoom -= .05
    if args[0] == 'z':
        zoom += .05
    if args[0] == 'w':
        xspeed -= .01
    if args[0] == 's':
        xspeed += .01
    if args[0] == 'd':
        yspeed -= .01
    if args[0] == 'a':
        yspeed += .01
    if args[0] == 'p':
        glutFullScreen()
    if args[0] == ESCAPE:
        glutDestroyWindow(window)
        sys.exit()
    if args[0] == '\t':
        for ind, particle in enumerate(particles):
            particle.x = 0
            particle.y = 0
            particle.z = 0
            particle.xi = random.random()*50 - 26
            particle.yi = random.random()*50 - 25
            particle.zi = random.random()*50 - 25
    if args[0] == '8':
        for ind, particle in enumerate(particles):
            if particle.yg < 1.5:
                particle.yg += 0.01
    if args[0] == '2':
        for ind, particle in enumerate(particles):
            if particle.yg > -1.5:
                particle.yg -= 0.01
    if args[0] == '6':
        for ind, particle in enumerate(particles):
            if particle.xg < 1.5:
                particle.xg += 0.01
    if args[0] == '4':
        for ind, particle in enumerate(particles):
            if particle.xg > -1.5:
                particle.xg -= 0.01


def special_key_pressed(*args):
    global textures, filter_type, light, zoom, xspeed, yspeed
    if args[0] == 101:
        xspeed -= .01
    if args[0] == 103:
        xspeed += .01
    if args[0] == 102:
        yspeed -= .01
    if args[0] == 100:
        yspeed += .01


def main():
    global window
    glutInit("")

    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH)

    glutInitWindowSize(800, 600)

    glutInitWindowPosition(0, 0)

    window = glutCreateWindow("lesson 7")

    glutDisplayFunc(draw_gl_scene)

    # Раскоментировать для полноэкранного режима
    # glutFullScreen()

    glutIdleFunc(draw_gl_scene)

    glutReshapeFunc(resize_gl_scene)

    glutKeyboardFunc(key_pressed)
    glutSpecialFunc(special_key_pressed)

    init_gl(800, 600)

    glutMainLoop()

print "Hit ESC key to quit."
main()