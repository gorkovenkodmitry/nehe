#coding: utf-8
from OpenGL.GL import *
from OpenGL.GL.VERSION.GL_1_1 import glGenTextures
from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.raw.GL.VERSION.GL_1_1 import glBindTexture
from PIL import Image

ESCAPE = '\033'

window = 0
textures = []
light = False
blend = False
xrot = yrot = xspeed = yspeed = 0
z = -5
light_ambient = [.5, .5, .5, 1]
light_diffuse = [1, 1, 1, 1]
light_position = [0, 0, 2, 1]
filter_type = 0


def load_textures(filename, filter_type):
    img = Image.open(filename)
    size = img.size
    img = img.convert('RGBA').tostring('raw', 'RGBA', 0, -1)
    texture = glGenTextures(1)
    glBindTexture(GL_TEXTURE_2D, texture)
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
    if filter_type == 0:
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    elif filter_type == 1:
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    elif filter_type == 2:
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST)
    else:
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    # glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL)
    if filter_type != 2:
        glTexImage2D(GL_TEXTURE_2D, 0, 4, size[0], size[1], 0, GL_RGBA, GL_UNSIGNED_BYTE, img)
    else:
        gluBuild2DMipmaps(GL_TEXTURE_2D, 4, size[0], size[1], GL_RGBA, GL_UNSIGNED_BYTE, img)
    return texture


def init_gl(width, height):
    global textures, filter_type, light_ambient, light_diffuse, light_position
    for i in range(3):
        textures.append(load_textures('glass.bmp', i))
    glEnable(GL_TEXTURE_2D)
    glClearColor(0.0, 0.0, 0.0, 0.0)
    glClearDepth(1.0)
    glEnable(GL_DEPTH_TEST)
    glDepthFunc(GL_LEQUAL)
    glShadeModel(GL_SMOOTH)
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST)

    # свет
    glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient)
    glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffuse)
    glLightfv(GL_LIGHT1, GL_POSITION, light_position)
    glEnable(GL_LIGHT1)

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0, float(width)/float(height), 0.1, 100.0)

    glMatrixMode(GL_MODELVIEW)


def resize_gl_scene(width, height):
    if height == 0:
        height = 1

    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0, float(width)/float(height), 0.1, 100.0)
    glMatrixMode(GL_MODELVIEW)


def draw_gl_scene():
    global xrot, yrot, xspeed, yspeed, z, textures, filter_type
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    glLoadIdentity()
    glTranslatef(0, 0, z)
    # куб
    glRotatef(xrot, 1, 0, 0)
    glRotatef(yrot, 0, 1, 0)
    glColor4f(1, 1, 1, .5)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE)
    glBindTexture(GL_TEXTURE_2D, textures[filter_type])
    glBegin(GL_QUADS)

    glTexCoord2f(0.0, 0.0)
    glVertex3f(-1.0, -1.0,  1.0)
    glTexCoord2f(1.0, 0.0)
    glVertex3f( 1.0, -1.0,  1.0)
    glTexCoord2f(1.0, 1.0)
    glVertex3f( 1.0,  1.0,  1.0)
    glTexCoord2f(0.0, 1.0)
    glVertex3f(-1.0,  1.0,  1.0)

    glTexCoord2f(1.0, 0.0)
    glVertex3f(-1.0, -1.0, -1.0)
    glTexCoord2f(1.0, 1.0)
    glVertex3f(-1.0,  1.0, -1.0)
    glTexCoord2f(0.0, 1.0)
    glVertex3f( 1.0,  1.0, -1.0)
    glTexCoord2f(0.0, 0.0)
    glVertex3f( 1.0, -1.0, -1.0)

    glTexCoord2f(0.0, 1.0)
    glVertex3f(-1.0,  1.0, -1.0)
    glTexCoord2f(0.0, 0.0)
    glVertex3f(-1.0,  1.0,  1.0)
    glTexCoord2f(1.0, 0.0)
    glVertex3f( 1.0,  1.0,  1.0)
    glTexCoord2f(1.0, 1.0)
    glVertex3f( 1.0,  1.0, -1.0)

    glTexCoord2f(1.0, 1.0)
    glVertex3f(-1.0, -1.0, -1.0)
    glTexCoord2f(0.0, 1.0)
    glVertex3f( 1.0, -1.0, -1.0)
    glTexCoord2f(0.0, 0.0)
    glVertex3f( 1.0, -1.0,  1.0)
    glTexCoord2f(1.0, 0.0)
    glVertex3f(-1.0, -1.0,  1.0)

    glTexCoord2f(1.0, 0.0)
    glVertex3f( 1.0, -1.0, -1.0)
    glTexCoord2f(1.0, 1.0)
    glVertex3f( 1.0,  1.0, -1.0)
    glTexCoord2f(0.0, 1.0)
    glVertex3f( 1.0,  1.0,  1.0)
    glTexCoord2f(0.0, 0.0)
    glVertex3f( 1.0, -1.0,  1.0)

    glTexCoord2f(0.0, 0.0)
    glVertex3f(-1.0, -1.0, -1.0)
    glTexCoord2f(1.0, 0.0)
    glVertex3f(-1.0, -1.0,  1.0)
    glTexCoord2f(1.0, 1.0)
    glVertex3f(-1.0,  1.0,  1.0)
    glTexCoord2f(0.0, 1.0)
    glVertex3f(-1.0,  1.0, -1.0)

    glEnd()
    glutSwapBuffers()
    xrot += xspeed
    yrot += yspeed


def key_pressed(*args):
    global textures, filter_type, light, z, xspeed, yspeed, blend
    if args[0] == 'f':
        filter_type = (filter_type + 1) % 3
    if args[0] == 'l':
        light = not light
        if light:
            glEnable(GL_LIGHTING)
        else:
            glDisable(GL_LIGHTING)
    if args[0] == 'x':
        z -= .02
    if args[0] == 'z':
        z += .02
    if args[0] == 'w':
        xspeed -= .01
    if args[0] == 's':
        xspeed += .01
    if args[0] == 'd':
        yspeed -= .01
    if args[0] == 'a':
        yspeed += .01
    if args[0] == 'p':
        glutFullScreen()
    if args[0] == 'b':
        blend = not blend
        if blend:
            glEnable(GL_BLEND)
            glDisable(GL_DEPTH_TEST)
        else:
            glDisable(GL_BLEND)
            glEnable(GL_DEPTH_TEST)
    if args[0] == ESCAPE:
        glutDestroyWindow(window)
        sys.exit()


def main():
    global window
    glutInit("")

    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH)

    glutInitWindowSize(640, 480)

    glutInitWindowPosition(0, 0)

    window = glutCreateWindow("lesson 8")

    glutDisplayFunc(draw_gl_scene)

    # Раскоментировать для полноэкранного режима
    # glutFullScreen()

    glutIdleFunc(draw_gl_scene)

    glutReshapeFunc(resize_gl_scene)

    glutKeyboardFunc(key_pressed)

    init_gl(640, 480)

    glutMainLoop()

print "Hit ESC key to quit."
main()