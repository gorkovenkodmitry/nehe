#coding: utf-8
import sys
from OpenGL.GL import *
from OpenGL.GL.VERSION.GL_1_1 import glGenTextures
from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GLUT.special import glutSpecialFunc
from PIL import Image

ESCAPE = '\033'

window = 0
texture = None
rquad = 0
box = None
top = None
xrot = yrot = 0
zoom = -15
box_colors = [
    [1, 0, 0],
    [1, .5, 0],
    [1, 1, 0],
    [0, 1, 0],
    [0, 1, 1],
]
top_colors = [
    [.5, 0, 0],
    [.5, .25, 0],
    [.5, .5, 0],
    [0, .5, 0],
    [0, .5, .5],
]


def load_textures(filename, filter_type=1):
    img = Image.open(filename)
    size = img.size
    img = img.convert('RGBA').tostring('raw', 'RGBA', 0, -1)
    texture = glGenTextures(1)
    glBindTexture(GL_TEXTURE_2D, texture)
    if filter_type == 0:
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    elif filter_type == 1:
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    elif filter_type == 2:
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST)
    else:
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    # glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL)
    if filter_type != 2:
        glTexImage2D(GL_TEXTURE_2D, 0, 4, size[0], size[1], 0, GL_RGBA, GL_UNSIGNED_BYTE, img)
    else:
        gluBuild2DMipmaps(GL_TEXTURE_2D, 4, size[0], size[1], GL_RGBA, GL_UNSIGNED_BYTE, img)
    return texture


def build_lists():
    global box, top
    box = glGenLists(2)
    glNewList(box, GL_COMPILE)

    glBegin(GL_QUADS)

    glTexCoord2f(1, 1)
    glVertex3f(1, -1,  1)
    glTexCoord2f(0, 1)
    glVertex3f(-1, -1,  1)
    glTexCoord2f(0, 0)
    glVertex3f(-1, -1,  -1)
    glTexCoord2f(1, 0)
    glVertex3f(1, -1,  -1)

    glTexCoord2f(1, 1)
    glVertex3f(1, 1, 1)
    glTexCoord2f(0, 1)
    glVertex3f(-1, 1, 1)
    glTexCoord2f(0, 0)
    glVertex3f(-1, -1, 1)
    glTexCoord2f(1, 0)
    glVertex3f(1, -1, 1)

    glTexCoord2f(1, 1)
    glVertex3f(1, 1, -1)
    glTexCoord2f(0, 1)
    glVertex3f(-1, 1, -1)
    glTexCoord2f(0, 0)
    glVertex3f(-1, -1, -1)
    glTexCoord2f(1, 0)
    glVertex3f(1, -1, -1)

    glTexCoord2f(1, 1)
    glVertex3f(1, 1, -1)
    glTexCoord2f(0, 1)
    glVertex3f(1, 1, 1)
    glTexCoord2f(0, 0)
    glVertex3f(1, -1, 1)
    glTexCoord2f(1, 0)
    glVertex3f(1, -1, -1)

    glTexCoord2f(1, 1)
    glVertex3f(-1, 1, -1)
    glTexCoord2f(0, 1)
    glVertex3f(-1, 1, 1)
    glTexCoord2f(0, 0)
    glVertex3f(-1, -1, 1)
    glTexCoord2f(1, 0)
    glVertex3f(-1, -1, -1)

    glEnd()
    glEndList()

    top = box + 1
    glNewList(top, GL_COMPILE)

    glBegin(GL_QUADS)

    glTexCoord2f(1, 1)
    glVertex3f(1, 1,  1)
    glTexCoord2f(0, 1)
    glVertex3f(-1, 1,  1)
    glTexCoord2f(0, 0)
    glVertex3f(-1, 1,  -1)
    glTexCoord2f(1, 0)
    glVertex3f(1, 1,  -1)

    glEnd()
    glEndList()


def init_gl(width, height):
    global texture
    build_lists()
    texture = load_textures('Cube.bmp')
    glEnable(GL_TEXTURE_2D)
    glShadeModel(GL_SMOOTH)
    glClearColor(0, 0, 0, 0.5)
    glClearDepth(1.0)
    glEnable(GL_DEPTH_TEST)
    glDepthFunc(GL_LEQUAL)

    glEnable(GL_LIGHT0)
    glEnable(GL_LIGHTING)
    glEnable(GL_COLOR_MATERIAL)
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST)



def resize_gl_scene(width, height):
    if height == 0:
        height = 1

    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0, float(width)/float(height), 0.1, 100.0)
    glMatrixMode(GL_MODELVIEW)


def draw_gl_scene():
    global rquad, texture, box, top, box_colors, top_colors
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    glBindTexture(GL_TEXTURE_2D, texture)
    for y in range(1, 6):
        for x in range(0, y):
            glLoadIdentity()
            glTranslatef(0, 0, -8)
            glTranslatef(1.4+(float(x)*2.8)-(float(y)*1.4), ((6.0-float(y))*2.4)-7.0, -20.0)
            glRotatef(45.0-(2.0*y)+xrot, 1, 0, 0)
            glRotatef(45.0+yrot, 0, 1, 0)
            glColor3fv(box_colors[y-1])
            glCallList(box)
            glColor3fv(top_colors[y-1])
            glCallList(top)
    glutSwapBuffers()


def key_pressed(*args):
    if args[0] == ESCAPE:
        glutDestroyWindow(window)
        sys.exit()


def key_special_pressed(*args):
    global yrot, xrot
    if args[0] == 1:
        glutFullScreen()
    if args[0] == 100:
        # left
        yrot -= .2
    if args[0] == 101:
        # up
        xrot -= .2
    if args[0] == 102:
        # right
        yrot += .2
    if args[0] == 103:
        # down
        xrot += .2


def main():
    global window
    glutInit("")

    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH)

    glutInitWindowSize(640, 480)

    glutInitWindowPosition(0, 0)

    window = glutCreateWindow("lesson 6")

    glutDisplayFunc(draw_gl_scene)

    glutIdleFunc(draw_gl_scene)

    glutReshapeFunc(resize_gl_scene)

    glutKeyboardFunc(key_pressed)
    glutSpecialFunc(key_special_pressed)

    init_gl(640, 480)

    glutMainLoop()

print "Hit ESC key to quit."
main()