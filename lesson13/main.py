#coding: utf-8
import glFreeType
from math import cos, sin
from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *
from PIL import Image

ESCAPE = '\033'

window = 0
base_font = None


def buld_font():
    global base_font
    base_font = glFreeType.FontData('arial.ttf', 32)


def load_textures(filename, filter_type=1):
    img = Image.open(filename)
    size = img.size
    img = img.convert('RGBA').tostring('raw', 'RGBA', 0, -1)
    texture = glGenTextures(1)
    glBindTexture(GL_TEXTURE_2D, texture)
    if filter_type == 0:
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    elif filter_type == 1:
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    elif filter_type == 2:
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST)
    else:
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    # glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL)
    if filter_type != 2:
        glTexImage2D(GL_TEXTURE_2D, 0, 4, size[0], size[1], 0, GL_RGBA, GL_UNSIGNED_BYTE, img)
    else:
        gluBuild2DMipmaps(GL_TEXTURE_2D, 4, size[0], size[1], GL_RGBA, GL_UNSIGNED_BYTE, img)
    return texture


def init_gl(width, height):
    glClearColor(0.0, 0.0, 0.0, 0.0)
    glClearDepth(1.0)
    glDepthFunc(GL_LESS)
    glEnable(GL_DEPTH_TEST)
    glShadeModel(GL_SMOOTH)

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0, float(width)/float(height), 0.1, 100.0)
    buld_font()

    glMatrixMode(GL_MODELVIEW)


def resize_gl_scene(width, height):
    if height == 0:
        height = 1

    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0, float(width)/float(height), 0.1, 100.0)
    glMatrixMode(GL_MODELVIEW)

cnt1 = 0
cnt2 = 0


def draw_gl_scene():
    global cnt1, cnt2, base_font
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glLoadIdentity()
    glTranslatef (0.0, 0.0, -1.0)       # // Move One Unit Into The Screen

    # // Pulsing Colors Based On Text Position
    glColor3f (1.0 * cos (cnt1), 1.0 * sin (cnt2), 1.0 -0.5 * cos (cnt1+cnt2))
    # // Position The Text On The Screen
    glRasterPos2f(-0.45+0.05* cos(cnt1), 0.32*sin(cnt2))
    base_font.glPrint (50, 240, "Active FreeType Text - %7.2f" % (cnt1))
    cnt1+=0.051                                        # // Increase The First Counter
    cnt2+=0.005                                        # // Increase The First Counter
    glutSwapBuffers()
    glutSwapBuffers()


def key_pressed(*args):
    if args[0] == ESCAPE:
        glutDestroyWindow(window)
        sys.exit()


def special_key_pressed(*args):
    print args[0]


def main():
    global window
    glutInit("")

    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH)

    glutInitWindowSize(640, 480)

    glutInitWindowPosition(0, 0)

    window = glutCreateWindow("full")

    glutDisplayFunc(draw_gl_scene)

    # Раскоментировать для полноэкранного режима
    # glutFullScreen()

    glutIdleFunc(draw_gl_scene)

    glutReshapeFunc(resize_gl_scene)

    glutKeyboardFunc(key_pressed)
    glutSpecialFunc(special_key_pressed)

    init_gl(640, 480)

    glutMainLoop()

print "Hit ESC key to quit."
main()