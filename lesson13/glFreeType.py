#coding: utf-8
from OpenGL.GL import *
from OpenGL.GLU import *
from PIL import ImageFont


def is_font_available (ft, facename):
    if facename in ft.available_fonts ():
        return True
    return False


def next_p2(num):
    rval = 1
    while rval < num:
        rval <<= 1
    return rval


def make_dlist(ft, ch, list_base, tex_base_list):
    glyph = ft.getmask (chr (ch))
    glyph_width, glyph_height = glyph.size
    width = next_p2 (glyph_width + 1)
    height = next_p2 (glyph_height + 1)

    expanded_data = ""
    for j in xrange (height):
        for i in xrange (width):
            if (i >= glyph_width) or (j >= glyph_height):
                value = chr (0)
                expanded_data += value
                expanded_data += value
            else:
                value = chr (glyph.getpixel ((i, j)))
                expanded_data += value
                expanded_data += value

    ID = glGenTextures (1)
    tex_base_list [ch] = ID
    glBindTexture (GL_TEXTURE_2D, ID)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)

    border = 0
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, width, height,
        border, GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, expanded_data )

    expanded_data = None

    glNewList (list_base + ch, GL_COMPILE)

    if ch == ord (" "):
        glyph_advance = glyph_width
        glTranslatef(glyph_advance, 0, 0)
        glEndList()
    else:

        glBindTexture (GL_TEXTURE_2D, ID)

        glPushMatrix()

        x=float (glyph_width) / float (width)
        y=float (glyph_height) / float (height)

        glBegin(GL_QUADS)
        glTexCoord2f(0,0), glVertex2f(0,glyph_height)
        glTexCoord2f(0,y), glVertex2f(0,0)
        glTexCoord2f(x,y), glVertex2f(glyph_width,0)
        glTexCoord2f(x,0), glVertex2f(glyph_width, glyph_height)
        glEnd()
        glPopMatrix()

        glTranslatef(glyph_width + 0.75, 0, 0)

        glEndList()

    return


def pushScreenCoordinateMatrix():
    glPushAttrib(GL_TRANSFORM_BIT)
    viewport = glGetIntegerv(GL_VIEWPORT)
    glMatrixMode(GL_PROJECTION)
    glPushMatrix()
    glLoadIdentity()
    gluOrtho2D(viewport[0],viewport[2],viewport[1],viewport[3])
    glPopAttrib()
    return


def pop_projection_matrix():
    glPushAttrib(GL_TRANSFORM_BIT)
    glMatrixMode(GL_PROJECTION)
    glPopMatrix()
    glPopAttrib()
    return


class FontData:

    def __init__(self, facename, pixel_height):
        self.m_allocated = False
        self.m_font_height = pixel_height
        self.m_facename = facename

        try:
            ft = ImageFont.truetype(facename, pixel_height)
        except:
            raise ValueError, "Unable to locate true type font '%s'" % (facename)

        self.m_list_base = glGenLists(128)

        self.textures = [None] * 128

        for i in xrange(128):
            make_dlist(ft, i, self.m_list_base, self.textures);

        self.m_allocated = True

        ft = None
        return

    def glPrint(self, x, y, string):
        pushScreenCoordinateMatrix()

        h = float(self.m_font_height) / 0.63

        if string == None:
            pop_projection_matrix()
            return
        if string == "":
            pop_projection_matrix()
            return

        lines = string.split ("\n")

        glPushAttrib(GL_LIST_BIT | GL_CURRENT_BIT  | GL_ENABLE_BIT | GL_TRANSFORM_BIT)
        glMatrixMode(GL_MODELVIEW)
        glDisable(GL_LIGHTING)
        glEnable(GL_TEXTURE_2D)
        glDisable(GL_DEPTH_TEST)
        glEnable(GL_BLEND)
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

        glListBase(self.m_list_base)
        modelview_matrix = glGetFloatv(GL_MODELVIEW_MATRIX)

        for i in xrange (len (lines)):
            line = lines [i]

            glPushMatrix ()
            glLoadIdentity ()
            glTranslatef (x,y-h*i,0)
            glMultMatrixf(modelview_matrix)

            glCallLists(line)
            glPopMatrix()

        glPopAttrib()
        pop_projection_matrix()
        return

    def release(self):
        if (self.m_allocated):
            glDeleteLists ( self.m_list_base, 128)
            for ID in self.textures:
                glDeleteTextures (ID)
            self.list_base = None
            self.m_allocated = False
        return

    def __del__ (self):
        self.release()
        return


# Unit Test harness if this python module is run directly.
if __name__ == "__main__":
    print "testing availability of freetype font arial\n"
    ft = ImageFont.truetype ("arial.ttf", 15)
    if ft:
        print "Found the TrueType font 'arial.ttf'"
    else:
        print "faild to find the TrueTYpe font 'arial'\n"
