#coding: utf-8
from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *

ESCAPE = '\033'

window = 0
rtri = 0
rquad = 0


def init_gl(width, height):
    glClearColor(0.0, 0.0, 0.0, 0.0)
    glClearDepth(1.0)
    glDepthFunc(GL_LESS)
    glEnable(GL_DEPTH_TEST)
    glShadeModel(GL_SMOOTH)

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0, float(width)/float(height), 0.1, 100.0)

    glMatrixMode(GL_MODELVIEW)


def resize_gl_scene(width, height):
    if height == 0:
        height = 1

    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0, float(width)/float(height), 0.1, 100.0)
    glMatrixMode(GL_MODELVIEW)


def draw_gl_scene():
    global rtri, rquad
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glLoadIdentity()
    glTranslatef(-1.5, 0, -6)
    # треугольник
    glRotatef(rtri, 0, 1, 0)
    glBegin(GL_TRIANGLES)
    glColor3f(1.0, 0.0, 0.0)
    glVertex3f(0.0, 1.0, 0.0)
    glColor3f(0.0, 1.0, 0.0)
    glVertex3f(-1.0, -1.0, 0.0)
    glColor3f(0.0, 0.0, 1.0)
    glVertex3f(1.0, -1.0, 0.0)
    glEnd()
    glLoadIdentity()
    glTranslatef(1.5, 0, -6)
    # квадрат
    glRotatef(rquad, 1, 0, 0)
    glBegin(GL_QUADS)
    glColor3f(0.5, 0.5, 1.0)
    glVertex3f(-1.0, 1.0, 0.0)
    glVertex3f(1.0, 1.0, 0.0)
    glVertex3f(1.0, -1.0, 0.0)
    glVertex3f(-1.0, -1.0, 0.0)
    glEnd()
    glutSwapBuffers()
    rtri += 0.2
    rquad -= 0.15


def key_pressed(*args):
    if args[0] == ESCAPE:
        glutDestroyWindow(window)
        sys.exit()


def main():
    global window
    glutInit("")

    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH)

    glutInitWindowSize(640, 480)

    glutInitWindowPosition(0, 0)

    window = glutCreateWindow("lesson 4")

    glutDisplayFunc(draw_gl_scene)

    # Раскоментировать для полноэкранного режима
    glutFullScreen()

    glutIdleFunc(draw_gl_scene)

    glutReshapeFunc(resize_gl_scene)

    glutKeyboardFunc(key_pressed)

    init_gl(640, 480)

    glutMainLoop()

print "Hit ESC key to quit."
main()