#coding: utf-8
import math
from OpenGL.GL import *
from OpenGL.GL.VERSION.GL_1_1 import glGenTextures
from OpenGL.GLUT import *
from OpenGL.GLU import *
from PIL import Image

ESCAPE = '\033'

window = 0
texture = None
xrot = yrot = zrot = 0
zoom = -12
wiggle_count = 0
points = []


def load_textures(filename, filter_type=0):
    img = Image.open(filename)
    size = img.size
    img = img.convert('RGBA').tobytes('raw', 'RGBA', 0, -1)
    texture = glGenTextures(1)
    glBindTexture(GL_TEXTURE_2D, texture)
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
    if filter_type == 0:
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    elif filter_type == 1:
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    elif filter_type == 2:
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST)
    else:
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL)
    if filter_type != 2:
        glTexImage2D(GL_TEXTURE_2D, 0, 4, size[0], size[1], 0, GL_RGBA, GL_UNSIGNED_BYTE, img)
    else:
        gluBuild2DMipmaps(GL_TEXTURE_2D, 4, size[0], size[1], GL_RGBA, GL_UNSIGNED_BYTE, img)
    return texture


def init_gl(width, height):
    global texture
    texture = load_textures('Tim.bmp', 2)
    glEnable(GL_TEXTURE_2D)
    glDisable(GL_LIGHTING)

    glClearColor(0.0, 0.0, 0.0, 0.0)
    glClearDepth(1.0)
    glDepthFunc(GL_LESS)
    glEnable(GL_DEPTH_TEST)
    glShadeModel(GL_SMOOTH)

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0, float(width)/float(height), 0.1, 100.0)

    glMatrixMode(GL_MODELVIEW)

    # glPolygonMode(GL_BACK, GL_FILL)
    # glPolygonMode(GL_FRONT, GL_LINE)
    for x in range(45):
        points.append([])
        for y in range(45):
            points[x].append([0, 0, 0])
            points[x][y][0] = x / 5.0 - 4.5
            points[x][y][1] = y / 5.0 - 4.5
            points[x][y][2] = math.sin((((x / 5.0) * 40.0) / 360.0) * math.pi * 2.0)


def resize_gl_scene(width, height):
    if height == 0:
        height = 1

    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0, float(width)/float(height), 0.1, 100.0)
    glMatrixMode(GL_MODELVIEW)


def draw_gl_scene():
    global points, xrot, yrot, zrot, zoom
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glLoadIdentity()
    glTranslatef(0, 0, zoom)
    glRotatef(xrot, 1, 0, 0)
    glRotatef(yrot, 0, 1, 0)
    glRotatef(zrot, 0, 0, 1)
    glBegin(GL_QUADS)
    for x in range(len(points) - 1):
        for y in range(len(points[x]) - 1):
            float_x = x / 44.0
            float_y = y / 44.0
            float_xb = (x + 1) / 44.0
            float_yb = (y + 1) / 44.0

            glTexCoord2f(float_x, float_y)
            glVertex3f(points[x][y][0], points[x][y][1], points[x][y][2])

            glTexCoord2f(float_x, float_yb)
            glVertex3f(points[x][y + 1][0], points[x][y + 1][1], points[x][y + 1][2])

            glTexCoord2f(float_xb, float_yb)
            glVertex3f(points[x + 1][y + 1][0], points[x + 1][y + 1][1], points[x + 1][y + 1][2])

            glTexCoord2f(float_xb, float_y)
            glVertex3f(points[x + 1][y][0], points[x + 1][y][1], points[x + 1][y][2])
    glEnd()

    hold = points[0]
    for x in range(len(points) - 1):
        for y in range(len(points[x])):
            points[x][y][2] = points[x + 1][y][2]
    for y in range(len(points[-1])):
        points[-1][y][2] = hold[y][2]

    xrot += .3
    yrot += .2
    zrot += .4
    glutSwapBuffers()


def key_pressed(*args):
    if args[0] == ESCAPE:
        glutDestroyWindow(window)
        sys.exit()


def special_key_pressed(*args):
    global zoom
    if args[0] == 104:
        zoom += .1
    if args[0] == 105:
        zoom -= .1


def main():
    global window
    glutInit("")

    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH)

    glutInitWindowSize(640, 480)

    glutInitWindowPosition(0, 0)

    window = glutCreateWindow("lesson 6")

    glutDisplayFunc(draw_gl_scene)

    # Раскоментировать для полноэкранного режима
    glutFullScreen()

    glutIdleFunc(draw_gl_scene)

    glutReshapeFunc(resize_gl_scene)

    glutKeyboardFunc(key_pressed)
    glutSpecialFunc(special_key_pressed)

    init_gl(640, 480)

    glutMainLoop()

print "Hit ESC key to quit."
main()